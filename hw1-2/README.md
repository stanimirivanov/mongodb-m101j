#Homework: Homework 1.2

Which of the following are valid JSON documents? Please choose all that apply.

* {�name�:�Fred Flintstone�;�occupation�:�Miner�;�wife�:�Wilma�}
* {�title�:�Star Wars�, �quotes�:[�Use The Force�,�These are not the Droids you are looking for�],�director�:�George Lucas�}
* {}
* {�city�:�New York�, �population�, 7999034, boros:{�queens�, �manhattan�, �staten island�, �the bronx�, �brooklyn�}}
* {�a�:1, �b�:{�b�:1, �c�:�foo�, �d�:�bar�, �e�:[1,2,4]}}

##Solution

* {�name�:�Fred Flintstone�;�occupation�:�Miner�;�wife�:�Wilma�}
	* __NO__ ; is not a valid separator
* {�title�:�Star Wars�, �quotes�:[�Use The Force�,�These are not the Droids you are looking for�],�director�:�George Lucas�}
	* __YES__
* {}
	* __YES__
* {�city�:�New York�, �population�, 7999034, boros:{�queens�, �manhattan�, �staten island�, �the bronx�, �brooklyn�}}
	* __NO__ �population�, 7999034, is not a valid entry
* {�a�:1, �b�:{�b�:1, �c�:�foo�, �d�:�bar�, �e�:[1,2,4]}}
	* __YES__