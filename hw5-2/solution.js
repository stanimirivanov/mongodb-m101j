db.zips.aggregate([
  { $match:{$or:[
        {state:"CA"},
        {state:"NY"}
    ] }},
  { $group: {
      _id: {"state": "$state", "city": "$city"},
      pop: {"$sum": "$pop" }
    }
  },
  { $match: {
      pop: { $gte: 25000 }
    }
  }, 
  { $group: {
      _id: "$state",
      average: {$avg: "$pop"}
    }
  }
])