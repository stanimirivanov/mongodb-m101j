package com.tengen;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 * Remove the grade of type "homework" with the lowest score 
 * for each student from the dataset that you imported in HW 2.1.
 *  
 * @class Week2Homework2
 * 
 * @brief Runs the HW 2." assignment.
 *
 */
public class Week2Homework2 {
	
    /**
     * @param args
     * @throws UnknownHostException
     */
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();

        DB database = client.getDB("students");
        DBCollection collection = database.getCollection("grades");
        
        long count = collection.count();
        System.out.println("Collection count: " + count);
        assert(count == 800);
        
        int nrDeleted = 0;
        
        DBObject query = new BasicDBObject("type", "homework");
        DBObject sortCriteria = new BasicDBObject("student_id", 1).append("score", 1);
        DBCursor cursor = collection.find(query).sort(sortCriteria);
        
        DBObject student = null;
        
        try {
            while (cursor.hasNext()) {
                DBObject nextStudent = cursor.next();
                if (student != null) {
					if (sameStudentId(student, nextStudent)) {
						DBObject lowestScore = findLowestScore(student, nextStudent);
						collection.remove(lowestScore);
						nrDeleted++;
					}
				}
                student = nextStudent;
            }
        } finally {
            cursor.close();
        }
        
        System.out.println("Collection count: " + (count - nrDeleted));
        assert(count - nrDeleted == 600);
    }
    
    /**
     * Compares two students and returns the <code>_id</code> of the one
     * with the lowest score.
     * 
     * @param first
     * @param second
     * 
     * @return
     */
    private static DBObject findLowestScore(DBObject first, DBObject second) {
    	double firstStudentScore = (Double) first.get("score");
    	double secondStudentScore = (Double) second.get("score");
    	return (firstStudentScore < secondStudentScore ? first : second);
	}
    
    /**
     * Returns <code>true</code> if the two students have the same 
     * <code>student_id</code>.
     * 
     * @param first
     * @param second
     * 
     * @return
     */
    private static boolean sameStudentId(DBObject first, DBObject second) {
    	int firstStudentId = (Integer) first.get("student_id");
    	int secondStudentId = (Integer) second.get("student_id");
    	return firstStudentId == secondStudentId;
	}
    
}
