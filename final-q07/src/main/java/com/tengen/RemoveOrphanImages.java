package com.tengen;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 * @class RemoveOrphanImages
 * 
 * @brief Runs the Final 7 assignment.
 *
 */
public class RemoveOrphanImages {
	
    /**
     * @param args
     * @throws UnknownHostException
     */
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();

        DB database = client.getDB("question7");
        DBCollection images = database.getCollection("images");
        DBCollection albums = database.getCollection("albums");
        
        DBCursor cursor = images.find();
        try {
            while (cursor.hasNext()) {
                DBObject nextImage = cursor.next();
                
                DBObject imageByIdQuery = new BasicDBObject("images", (Integer) nextImage.get("_id"));
                int references = albums.find(imageByIdQuery).count();
                if (references == 0) {
					images.remove(nextImage);
				}
            }
        } finally {
            cursor.close();
        }
    }
}
