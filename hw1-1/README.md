#Homework: Homework 1.1

Install MongoDB on your computer and run it on the standard port.
Download the HW1-1 from the Download Handout link and uncompress it.

Use mongorestore to restore the dump into your running mongod. Do this 
by opening a terminal window (mac) or cmd window (windows) and navigating 
to the directory so that the dump directory is directly beneath you. Now type

```
mongorestore dump
```
Note you will need to have your path setup correctly to find mongorestore.

Now, using the Mongo shell, perform a findone on the collection called 
*hw1* in the database *m101*. That will return one document. Please provide 
the value corresponding to the "answer" key from the document returned.

##Solution
```
> use m101
switched to db m101
> db.hw1.findOne()
{
        "_id" : ObjectId("50773061bf44c220307d8514"),
        "answer" : 42,
        "question" : "The Ultimate Question of Life, The Universe and Everything"
}
```