package com.tengen;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 * Remove the grade of type "homework" with the lowest score 
 * for each student.
 *  
 * @class Week3Homework1
 * 
 * @brief Runs the HW 1." assignment.
 *
 */
public class Week3Homework1 {
	
    /**
     * @param args
     * @throws UnknownHostException
     */
	@SuppressWarnings("unchecked")
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();

        DB database = client.getDB("school");
        DBCollection collection = database.getCollection("students");
        
        long count = collection.count();
        System.out.println("Collection count: " + count);
        assert(count == 200);
        
        DBObject sortCriteria = new BasicDBObject("student_id", 1).append("score", 1);
        DBCursor cursor = collection.find().sort(sortCriteria);
        
        Comparator<DBObject> scoreCcomparator = new Comparator<DBObject>() {
    		public int compare(DBObject o1, DBObject o2) {
    			Double score1 = (Double) o1.get("score");
    			Double score2 = (Double) o2.get("score");
    			return score1.compareTo(score2);
    		}
        };
		
        try {
            while (cursor.hasNext()) {
                DBObject nextStudent = cursor.next();
                
				List<DBObject> scores = (List<DBObject>) nextStudent.get("scores");
				List<DBObject> hwScores =  new ArrayList<DBObject>();
                
                for (DBObject score : scores) {
                	String type = (String) score.get("type");
                	if (type.equals("homework")) {
                		hwScores.add(score);
					}
				}
                
                
                Collections.sort(hwScores, scoreCcomparator);
                if (!hwScores.isEmpty()) {
                	DBObject lowestHWScore = hwScores.get(0);
                	scores.remove(lowestHWScore);
				}
                
                int id = (Integer) nextStudent.get("_id");
                DBObject query = new BasicDBObject("_id", id);
                DBObject updatedScores = new BasicDBObject("scores", scores);
                
                DBObject update = new BasicDBObject("$set", updatedScores);
                collection.update(query, update);
            }
        } finally {
            cursor.close();
        }
        
    }
    
}
