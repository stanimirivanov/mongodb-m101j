#Final: Question 8

Supposed we executed the following Java code. How many animals will be inserted into 
the "animals" collection?

```
public class Question8 {

    public static void main(String[] args) throws IOException {
        MongoClient c =  new MongoClient(new MongoClientURI("mongodb://localhost"));
        DB db = c.getDB("test");
        DBCollection animals = db.getCollection("animals");

        BasicDBObject animal = new BasicDBObject("animal", "monkey");

        animals.insert(animal);
        animal.removeField("animal");
        animal.append("animal", "cat");
        animals.insert(animal);
        animal.removeField("animal");
        animal.append("animal", "lion");
        animals.insert(animal);
    }
}
```

* 0
* 1
* 2
* 3

##Solution

Import the collection and perform the necessary check.
```
> use test
> db.animals.find().count()
0
```

Run `Question8` as a java program.
```
mvn compile exec:java -Dexec.mainClass=com.tengen.Question8
```
This will result in the following exception being thrown:
```
com.mongodb.MongoException$DuplicateKey: insertDocument :: caused by :: 11000 E11000 duplicate key error index: test.animals.$_id_  dup key: { : ObjectId('53bebb40cc1eb37ec2e386dd') }
```

Query again for all animals count.
```
> db.animals.find().count()
1
```
