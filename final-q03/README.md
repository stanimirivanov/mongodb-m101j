#Final: Question 3

In this problem you will update a document in the messages collection to illustrate your 
mastery of updating documents from the shell. In fact, we've created a collection with a 
very similar schema to the Enron dataset, but filled instead with randomly generated data.

Please add the email address "mrpotatohead@10gen.com" to the list of addresses in the 
"headers.To" array for the document with "headers.Message-ID" of 
"<8147308.1075851042335.JavaMail.evans@thyme>"

This is a fully functional web shell, so please press enter f

##Solution
```
db.messages.update({"headers.Message-ID": "<8147308.1075851042335.JavaMail.evans@thyme>"}, {$push: {"headers.To": "mrpotatohead@10gen.com"}})
```