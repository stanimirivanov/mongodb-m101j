#Final: Question 6

Suppose you have a collection of students of the following form:

```
{
	"_id" : ObjectId("50c598f582094fb5f92efb96"),
	"first_name" : "John",
	"last_name" : "Doe",
	"date_of_admission" : ISODate("2010-02-21T05:00:00Z"),
	"residence_hall" : "Fairweather",
	"has_car" : true,
	"student_id" : "2348023902",
	"current_classes" : [
		"His343",
		"Math234",
		"Phy123",
		"Art232"
	]
}
```

Now suppose that basic inserts into the collection, which only include the 
last name, first name and student_id, are too slow (we can't do enough of them 
per second from our program). What could potentially improve the speed of 
inserts. Check all that apply.

* Add an index on last_name, first_name if one does not already exist.
* Remove all indexes from the collection
* Provide a hint to MongoDB that it should not use an index for the inserts
* Set w=0, j=0 on writes
* Build a replica set and insert data into the secondary nodes to free up the primary nodes.

##Solution

* Add an index on last_name, first_name if one does not already exist.
	* __NO__ Adding an index has some negative performance impact for write operations. 
		For collections with high write-to-read ratio, indexes are expensive since each insert 
		must also update any indexes.
* Remove all indexes from the collection
	* __YES__ (see previous note)
* Provide a hint to MongoDB that it should not use an index for the inserts
	* __NO__
* Set w=0, j=0 on writes
	* __YES__ w=0 will disable basic acknowledgement of write operations, 
		j=0 will not wait for confirms that the mongod instance has written the 
		data to the on-disk journal.
* Build a replica set and insert data into the secondary nodes to free up the primary nodes.
	* __NO__ mongodb does not allow inserts of data to secondary nodes