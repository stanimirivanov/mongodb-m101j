#Homework: Homework 4.3 (MongoProc)

##Making the Blog fast

In this homework assignment you will be adding some indexes to the post collection to make the blog fast.

We are also providing a patriotic (if you are an American) data set for the blog. There are 1000 entries with lots of comments and tags. You must load this dataset to complete the problem.

```
# from the mongodb shell
use blog
db.posts.drop()
# from the terminal window
mongoimport -d blog -c posts < posts.json
```

The blog has been enhanced so that it can also display the top 10 most recent posts by tag. There are hyperlinks from the post tags to the page that displays the 10 most recent blog entries for that tag. (run the blog and it will be obvious)

Your assignment is to make the following blog pages fast:

* The blog home page
* The page that displays blog posts by tag (http://localhost:8082/tag/whatever)
* The page that displays a blog entry by permalink (http://localhost:8082/post/permalink)

By fast, we mean that indexes should be in place to satisfy these queries such that we only need to scan the number of documents we are going to return.

To figure out what queries you need to optimize, you can read the code and see what it does to display those pages. Isolate those queries and use explain to explore.

##Solution
```
use blog
db.posts.ensureIndex({date: -1})
db.posts.ensureIndex({permalink: 1})
db.posts.ensureIndex({tags:1, date:-1})
```